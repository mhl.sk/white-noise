<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Page title -->
    <title>Relax</title>

    <!-- Meta tags -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="/favicon.png" />
    
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

    <!-- Styling -->
    <link rel="stylesheet" href="/assets/css/style.css">

</head>
<body>
    <?php 
        $sounds = [
            "fire",
            "rain",
            "birds",
            "vacuum-cleaner",
            "thunder",
            "heartbeat",
            "river",
            "crickets",
            "hairdryer",
            "frogs"
        ];
    ?>
    <div class="navigation">
        <div class="container p-sm-0">
            <b>RELAX</b> | WHITE NOISE
            <div class="float-end">
                <a href="https://gitlab.com/mhl.sk/white-noise" class="text-black" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-git" viewBox="0 0 16 16">
                        <path d="M15.698 7.287 8.712.302a1.03 1.03 0 0 0-1.457 0l-1.45 1.45 1.84 1.84a1.223 1.223 0 0 1 1.55 1.56l1.773 1.774a1.224 1.224 0 0 1 1.267 2.025 1.226 1.226 0 0 1-2.002-1.334L8.58 5.963v4.353a1.226 1.226 0 1 1-1.008-.036V5.887a1.226 1.226 0 0 1-.666-1.608L5.093 2.465l-4.79 4.79a1.03 1.03 0 0 0 0 1.457l6.986 6.986a1.03 1.03 0 0 0 1.457 0l6.953-6.953a1.031 1.031 0 0 0 0-1.457"/>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="container pt-sm-4">
        <div class="d-xl-none explore-sounds" id="explore-sounds">
            <h2>Permission needed</h2>
            <p>To use this website on your smartphone, you need to give it permission to play sounds first.</p>
            <button class="btn btn-success" id="explore-btn">I agree</button>
        </div>
        <div class="row selection-holder" id="selection-holder">
            <?php foreach ($sounds as $key => $sound) { ?>
                <div class="col-6 col-md-4 col-lg-3 element-holder">
                    <img 
                        class="element-icon"
                        src="/assets/img/<?= $sound ?>.svg" 
                        alt="<?= $sound ?>"
                    >
                    <audio 
                        controls 
                        loop 
                        id="sound_<?= $key ?>"
                        hidden
                    >
                        <source src="/assets/sounds/<?= $sound ?>.mp3" type="audio/mp3">
                    </audio>
                    <input 
                        type="range" 
                        id="volume_<?= $key ?>"
                        min="0" 
                        max="1" 
                        step="0.01"
                        value="0"
                        class="slider"
                    >
                    <script>
                        var audio_<?= $key ?> = document.getElementById('sound_<?= $key ?>');
                        var volume_<?= $key ?> = document.getElementById('volume_<?= $key ?>');
                        volume_<?= $key ?>.addEventListener("input", function(){
                            audio_<?= $key ?>.play();
                            audio_<?= $key ?>.volume = volume_<?= $key ?>.value;
                        });
                    </script>
                </div>
            <?php } ?>
        </div>
    </div>
    <script>
        document.getElementById("explore-btn").addEventListener("click", function() {
            document.getElementById("explore-sounds").style.opacity = "0";
            setTimeout(() => {
                document.getElementById("explore-sounds").remove();
            }, 250);
        });
    </script>
</body>
</html>